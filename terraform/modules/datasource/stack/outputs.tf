output "domain" {
  value = local.domain_config
}

output "name" {
  value = local.name_config
}

output "tenant_stack_ids" {
  value = local.tenant_stack_ids
}

output "gcp" {
  value = local.gcp_config
}

output "tenant" {
  value = local.tenant_config
}
locals {
  no_pets = range(20)
  pet_ids = [ for id in local.no_pets : tostring(id) ]
  pet_store = [for pet in random_pet.pets : pet.id]
}

resource "random_pet" "pets" {
  for_each = toset(local.pet_ids)
}

resource "local_file" "pet_store" {
  filename = "/tmp/petstore.yaml"
  content = yamlencode(local.pet_store)
}
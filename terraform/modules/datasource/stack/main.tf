locals {
  resource_path = "${path.module}/resources"
  config_suffix = ".config.yaml"
  stack_ids = toset([ for file in fileset(local.resource_path, "*/*${local.config_suffix}") : dirname(file) ])
  stacks_raw = { for stack_id in local.stack_ids : stack_id => {
    for filePath in fileset("${local.resource_path}/${stack_id}", "*${local.config_suffix}") : trimsuffix(filePath, local.config_suffix) => yamldecode(file("${local.resource_path}/${stack_id}/${filePath}"))
  }}

  tenant_stack_ids = [ for stack_id, stack in local.stacks_raw : stack_id if try(stack["tenant"]["host"]["stack_id"] == var.stack_id, false) ]
  domain_config = try(local.stacks_raw[var.stack_id]["domain"], {})
  name_config = try(merge(local.stacks_raw[var.stack_id]["name"], {stack = var.stack_id}), {})
  gcp_config = try(local.stacks_raw[var.stack_id]["gcp"], {})
  tenant_config = try(local.stacks_raw[var.stack_id]["tenant"], {})
}
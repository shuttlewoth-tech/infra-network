#!/usr/bin/env zsh

check_bw_unlocked(){
    if bw unlock --check > /dev/null 2>&1
    then
      echo "Bitwarden is unlocked"
    else
      echo "Bitwarden is locked"
      exit 1
    fi
}

check_bw_unlocked

PROJECT_ID="40095325"
GITLAB_API_KEY_ID="366e5ee5-75eb-45d3-ac32-ad82012bf86c"

echo "Fetching GITLAB_ACCESS_TOKEN"
GITLAB_ACCESS_TOKEN=$(bw get item ${GITLAB_API_KEY_ID} | jq -r '.fields[0] .value')
export GITLAB_ACCESS_TOKEN

if [ -z "${STATE_NAME}" ]; then
  STATE_NAME="curious-pug"
  vared -r "STATE_NAME?" -c STATE_NAME
fi

terraform init -migrate-state \
    -backend-config="address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="username=aurelian-shuttleworth" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
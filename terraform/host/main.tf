module "stack_config" {
  source = "../modules/datasource/stack"
  stack_id = "curious-pug"
}

module "tenant_stack_config" {
  for_each = toset(module.stack_config.tenant_stack_ids)
  source = "../modules/datasource/stack"
  stack_id = each.value
}

resource "google_dns_managed_zone" "main" {
  dns_name = module.stack_config.domain["dns_name"]
  name = module.stack_config.domain["zone_name"]

  visibility = "public"
}

resource "google_dns_record_set" "root" {
  for_each = module.stack_config.domain["records"]
  managed_zone = google_dns_managed_zone.main.name
  name         = google_dns_managed_zone.main.dns_name
  type         = each.value["type"]
  ttl = each.value["ttl"]
  rrdatas = each.value["rrdatas"]
}

resource "google_dns_record_set" "tenant" {
  for_each = module.tenant_stack_config
  managed_zone = google_dns_managed_zone.main.name
  name = "${each.value.name["tenant"]}.${google_dns_managed_zone.main.dns_name}"
  type = "NS"
  ttl = 5
  rrdatas = each.value.tenant["host"]["managed_zone"]["name_servers"]
}
provider "google" {
  project = module.stack_config.gcp["project"]
  region = module.stack_config.gcp["region"]
  impersonate_service_account = module.stack_config.gcp["impersonate_service_account"]
}